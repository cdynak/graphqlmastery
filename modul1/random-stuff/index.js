const { networkInterfaces } = require('os');
const { ApolloServer, gql } = require("apollo-server");
const PORT = process.env.PORT || 4000;

const typeDefs = gql`
    type Query {
        greeting: String
        interestingUrls: [String]
        firstName: String
        email: String
        pets: [String]
        randomDiceThrow: Int
        fewRandomDiceThrows: [Int]
        counter: Int
        pi: Float
        e: Float
        eulerSeries: [Float]
        isTodayFriday: Boolean
        randomCoinTossessUntillTrue: [Boolean]
    }
`;

let count = 0;

const rootValue = () => {
    const getRandomDiceThrow = (sides = 6) => Math.ceil(Math.random() * sides);
    const getFewRandomDiceThrows = () => [...Array(2+getRandomDiceThrow(5))].map(() => Math.floor(getRandomDiceThrow()));
    const getCounter = () => count++;
    const getEulerSeries = (length = 1000) => [...Array(length)].map((a,i) => (1+1/(i+1))**(i+1));
    const today = new Date();
    const randomCoinToss = () => Math.random() > 0.5;
    const getRandomCoinTossessUntillTrue = () => {
        const result = [];
        do {
            result.push(randomCoinToss());
        } while(!result[result.length - 1]);
        return result;
    }

    return {
        greeting: "Hello world!",
        interestingUrls: ["https://gitlab.com/cdynak", "https://gitlab.dynak.com"],
        firstName: "John",
        email: "john@example.com",
        pets: ["Mittens", "Doggo", "Birb"],
        randomDiceThrow: getRandomDiceThrow(),
        fewRandomDiceThrows: getFewRandomDiceThrows(),
        counter: getCounter(),
        eulerSeries: getEulerSeries(),
        e: Math.E,
        pi: Math.PI,
        isTodayFriday: today.getDay() === 5,
        randomCoinTossessUntillTrue: getRandomCoinTossessUntillTrue()
    };
};
const server = new ApolloServer({ typeDefs, rootValue, playground: true, introspection: true });

console.log(networkInterfaces());
server.listen({ port: PORT }).then((result) => console.log(result.url));